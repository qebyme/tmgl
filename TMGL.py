# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 20:50:43 2021
"""

#%% import da modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#%% placement and round factors
factors_placement = [1.0, 0.8, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                     0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
Nplayers = len(factors_placement)
factors_rounds = [0.10, 0.20, 0.30, 0.40, 0.50, 1.00]
Nrounds = len(factors_rounds)


#%% player skill level approximations
# all players have an equal chance of winning
p_equal = np.ones(Nplayers)
# #1 is twice as likely to win as #2 and trice as likely to win as #3 etc.
p_inverse = [1 / (i + 1) for i in range(Nplayers)]
# (almost) certain what the outcome is gonna be
p_given = [10**(-2*i) for i in range(Nplayers)]
# Based on the 32 TMGL races; how likely is player to win a race
p_TMGL_tot = [1,0.784081938,0.78187683,0.748162256,0.70015101,0.693695692,0.624664151,0.577565568,0.455612526,0.492409284,0.440000692,0.314534242,0.244370068,0.200798158,0.195866681,0.178834051]
# Based on the 32 TMGL:C races; how likely is player to win a race
p_TMGLC_tot = [1,0.974624267,0.768294933,0.590704148,0.571229495,0.487043944,0.450880282,0.445150176,0.337163942,0.322519515,0.259679925,0.285966877,0.233925427,0.237225583,0.065783842,0.04209722]
# Expected ranking of a player on specific map, based on performance on that map after four complete races 
p_TMGL_map = [1,0.880554496,0.716107129,0.509924523,0.457425261,0.367301757,0.332658452,0.3095799,0.168731721,0.16189547,0.152504209,0.14153133,0.117898033,0.025702126,0.014744873,0.007038129]

# pick a method:
p = p_TMGL_map


#%% simulation
# number of simulations and make storage lists
Nsimulations = 1000
tops, lows, sums, meds = [], [], [], []

# do for every simulation round
for simul in range(Nsimulations):
    # make unit score for every player
    scores = np.ones(Nplayers)    
    # for all rounds do
    for rnd in range(Nrounds):
        # select placement ranking using corrolation
        placement = np.random.choice(np.arange(Nplayers), Nplayers, False, p = np.array(p) / np.sum(p))
        # for all players, calculate score
        for player in range(Nplayers):
            scores[player] *= factors_placement[placement[player]] * factors_rounds[rnd] + 1
            
    # save data to lists
    tops.append(np.max(scores))
    lows.append(np.min(scores))
    sums.append(np.sum(scores))
    meds.append(np.median(scores))

#%% plots
plt.figure()
plt.title("Lowest multiplier in round")
plt.hist(lows)

plt.figure()
plt.title("Median multiplier in round")
plt.hist(meds)

plt.figure()
plt.title("Highest multiplier in round")
plt.hist(tops)

plt.figure()
plt.title("Summation of the multipliers in round")
plt.hist(sums)

#%% prints
pd.set_option('display.max_columns', 14)
pd.set_option('display.width', 1000)
print(pd.DataFrame([
    [np.min(lows), np.percentile(lows, 5), np.percentile(lows, 25), np.median(lows), np.percentile(lows, 75), np.percentile(lows, 95), np.max(lows)],
    [np.min(meds), np.percentile(meds, 5), np.percentile(meds, 25), np.median(meds), np.percentile(meds, 75), np.percentile(meds, 95), np.max(meds)],
    [np.min(tops), np.percentile(tops, 5), np.percentile(tops, 25), np.median(tops), np.percentile(tops, 75), np.percentile(tops, 95), np.max(tops)],
    [np.min(sums), np.percentile(sums, 5), np.percentile(sums, 25), np.median(sums), np.percentile(sums, 75), np.percentile(sums, 95), np.max(sums)]], 
    columns=[f"{100/Nsimulations}% limit", "5% limit", "25% limit", "Median", "75% limit", "95% limit", f"{100 - 100/Nsimulations}% limit"],
    index=["Lowest multiplier", "Median multiplier", "Highest multiplier", "Sum of multipliers"]))
print(f"\nTheoretic max: {np.product(np.array(factors_rounds) + 1)}")
